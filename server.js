const express = require('express');
const app = express();
const port = 3000;
const mysql = require('mysql');
const bodyParser = require('body-parser')
var session = require('express-session')
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))

var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'blog'
});

connection.connect(function (err) {
	if (err) throw err;
	console.log("Connected!");
});

//app.get('/',(req,res) => res.send('hello world'));

//for person
app.get('/login', (req, res) => {
	res.render("login.pug", { title: "Express" });
})

app.post("/login", function (req, res, next) {
	var data = {
		email: req.body.email,
		pass: req.body.password
	};
	console.log(data);
	var sql = "SELECT * FROM MST_Person_Information WHERE email = ? AND password = ?";
	connection.query(sql, [data.email, data.pass], function (err, result) {
		if (result.length > 0) {
			console.log(result)
			connection.query('SELECT * FROM articles', (err, result2) => {
				req.session.status = result[0].status
				res.render('index', {
					articles: result2,
					status: req.session.status
				});
			})
			// res.render('result.pug', {
			// 	people: result
			// });
			// console.log(result)
		} else {
			res.render("notfound.pug", { title: "Express" });
		}
	});

});
// app.post("/login", function (req, res, next) {
// 	var data = {
// 		email: req.body.email,
// 		pass: req.body.password
// 	};
// 	console.log(data);
// 	var sql = "SELECT * FROM MST_Person_Information WHERE email = ? AND password = ?";
// 	connection.query(sql, [data.email, data.pass], function (err, result) {
// 		if (result.length > 0) {
// 			res.render('result.pug', {
// 				people: result
// 			});
// 			console.log(result)
// 		} else {
// 			res.render("notfound.pug", { title: "Express" });
// 		}
// 	});

// });


app.get('/person', (req, res) => {
	connection.query('SELECT * FROM MST_Person_Information', (err, result) => {
		res.render('person.pug', {
			people: result
		});
	})
});

app.get('/person/add', (req, res) => {
	res.render('addPerson.pug');
});

app.post('/person/add', (req, res) => {
	const name = req.body.name;
	const age = req.body.age;
	const movie = req.body.movie;
	const email = req.body.email;
	const password = req.body.password;
	const person = {
		name: name,
		age: age,
		movie: movie,
		email: email,
		password: password
	}
	connection.query('INSERT INTO MST_Person_Information SET ?', person, (err) => {
		console.log('Data Inserted');
		return res.redirect('/person');
	});
});

app.get('/person/edit/:id', (req, res) => {

	const edit_postID = req.params.id;

	connection.query('SELECT * FROM MST_Person_Information WHERE id=?', [edit_postID], (err, results) => {
		if (results) {
			res.render('editPerson.pug', {
				person: results[0]
			});
		}
	});
});

app.post('/person/edit/:id', (req, res) => {
	const name = req.body.name;
	const age = req.body.age;
	const movie = req.body.movie;
	const email = req.body.email;
	const password = req.body.password;
	const userId = req.params.id;
	connection.query('UPDATE `MST_Person_Information` SET name = ?, age = ?, movie = ?,email = ?, password = ? WHERE id = ?', [name, age, movie, email, password, userId], (err, results) => {
		if (results.changedRows === 1) {
			console.log('Post Updated');
		}
		return res.redirect('/person');
	});
});

app.get('/person/delete/:id', (req, res) => {
	connection.query('DELETE FROM `MST_Person_Information` WHERE id = ?', [req.params.id], (err, results) => {
		return res.redirect('/person');
	});
});


//for articles
app.get('/', (req, res) => {
	connection.query('SELECT * FROM articles', (err, result) => {
		res.render('index', {
			articles: result,
			status: req.session.status
		});
	})
});

app.get('/add', (req, res) => {
	res.render('add');
});

app.post('/add', (req, res) => {
	const title = req.body.title;
	const content = req.body.body;
	const article = {
		title: title,
		body: content,
		author_id: 1,
		update_ts: new Date()
	}
	connection.query('INSERT INTO articles SET ?', article, (err) => {
		console.log('Data Inserted');
		return res.redirect('/');
	});
});

app.get('/edit/:id', (req, res) => {

	const edit_postID = req.params.id;

	connection.query('SELECT * FROM articles WHERE atc_id=?', [edit_postID], (err, results) => {
		if (results) {
			res.render('edit', {
				articles: results[0]
			});
		}
	});
});

app.post('/edit/:id', (req, res) => {
	const update_title = req.body.title;
	const update_body = req.body.body;
	const update_ts = new Date()
	const userId = req.params.id;
	connection.query('UPDATE `articles` SET title = ?, body = ?, update_ts = ? WHERE atc_id = ?', [update_title, update_body, update_ts, userId], (err, results) => {
		if (results.changedRows === 1) {
			console.log('Post Updated');
		}
		return res.redirect('/');
	});
});

app.get('/delete/:id', (req, res) => {
	connection.query('DELETE FROM `articles` WHERE atc_id = ?', [req.params.id], (err, results) => {
		return res.redirect('/');
	});
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))